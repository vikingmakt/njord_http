from rask.base import Base
from rask.options import options

__all__ = ['Announce']

class Announce(Base):   
    def __init__(self,_):
        self.channel = _.result().channel
        
        self.ioengine.loop.add_callback(self.exchange_declare)
        self.log.info('started')

    def exchange_declare(self):
        def on_topic(*args):
            self.log.info('Exchange declare: %s' % options.http['rmq_exchange']['topic'])
            return True
        
        def on_headers(*args):
            self.log.info('Exchange declare: %s' % options.http['rmq_exchange']['headers'])
            self.channel.exchange_bind(
                destination=options.http['rmq_exchange']['headers'],
                source=options.http['rmq_exchange']['topic'],
                routing_key='#'
            )
            return True
        
        def on_headers_any(*args):
            self.log.info('Exchange declare: %s' % options.http['rmq_exchange']['headers_any'])
            self.channel.exchange_bind(
                destination=options.http['rmq_exchange']['headers_any'],
                source=options.http['rmq_exchange']['topic'],
                routing_key='#'
            )
            self.ioengine.loop.add_callback(self.queue_declare)
            return True

        self.channel.exchange_declare(
            callback=on_topic,
            exchange=options.http['rmq_exchange']['topic'],
            exchange_type='topic',
            durable=True
        )
        
        self.channel.exchange_declare(
            callback=on_headers,
            exchange=options.http['rmq_exchange']['headers'],
            exchange_type='headers',
            durable=True,
            arguments={
                'x-match':'all'
            }
        )
        
        self.channel.exchange_declare(
            callback=on_headers_any,
            exchange=options.http['rmq_exchange']['headers_any'],
            exchange_type='headers',
            durable=True,
            arguments={
                'x-match':'any'
            }
        )
        return True

    def queue_declare(self,_=None):
        try:
            service = _.next()

            def on_declare(*args):
                self.channel.queue_bind(
                    callback=None,
                    queue=options.http['services'][service]['queue'],
                    exchange=options.http['rmq_exchange']['topic'],
                    routing_key=options.http['services'][service]['rk']
                )
                return True

            self.channel.queue_declare(
                callback=on_declare,
                queue=options.http['services'][service]['queue'],
                durable=True,
                exclusive=False
            )
        except AttributeError:
            self.ioengine.loop.add_callback(
                self.queue_declare,
                iter(options.http['services'])
            )
        except StopIteration:
            return True
        except:
            raise
        else:
            self.log.info('Queue declare: %s' % options.http['services'][service]['queue'])
            self.ioengine.loop.add_callback(
                self.queue_declare,
                _=_
            )
        return None
