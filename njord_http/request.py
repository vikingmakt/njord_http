from rask.base import Base
from rask.options import options
from rask.parser.utcode import UTCode
from rask.rmq import BasicProperties
from tornado.httpclient import AsyncHTTPClient,HTTPRequest

__all__ = ['Request']

class Request(Base):
    def __init__(self,channel):
        self.channel = channel.result().channel
        self.client = AsyncHTTPClient(
            defaults={
                'user_agent':'Njord HTTP'
            }
        )

        self.channel.basic_consume(
            consumer_callback=self.on_msg,
            queue=options.http['services']['request']['queue']
        )
        self.log.info('started')

    def fetch(self,msg,properties,ack):
        def on_response(_):
            self.ioengine.loop.add_callback(
                self.response,
                msg=msg,
                properties=properties,
                ack=ack,
                response=_
            )
            return True

        try:
            self.client.fetch(HTTPRequest(**msg),callback=on_response)
        except:
            ack.set_result(True)
            raise
        return True   
        
    def on_msg(self,channel,method,properties,body):
        def ack(_):
            try:
                assert _.result()
            except AssertionError:
                channel.basic_nack(method.delivery_tag)
            except:
                raise
            else:
                channel.basic_ack(method.delivery_tag)
            return True

        def on_decode(_):
            self.ioengine.loop.add_callback(
                self.validate,
                msg=_.result(),
                properties=properties,
                ack=self.ioengine.future(ack)
            )
            return True

        UTCode().decode(body,future=self.ioengine.future(on_decode))
        return True

    def response(self,msg,properties,ack,response):
        properties.headers['code'] = options.http['code']['request']['ok']
        properties.headers['njord-http-response'] = True
        
        def on_encode(_):
            self.channel.basic_publish(
                body=_.result(),
                exchange=options.http['rmq_exchange']['topic'],
                properties=properties,
                routing_key=''
            )
            ack.set_result(True)
            return True

        UTCode().encode(
            {
                'code':response.code,
                'body':response.body,
                'headers':dict(response.headers),
                'request_time':response.request_time
            },
            future=self.ioengine.future(on_encode)
        )
        return True
    
    def validate(self,msg,properties,ack):
        try:
            assert msg['url']
        except (AssertionError,KeyError):
            ack.set_result(True)
        except:
            raise
        else:
            self.ioengine.loop.add_callback(
                self.fetch,
                msg=msg,
                properties=properties,
                ack=ack
            )
        return True
