from rask.base import Base
from rask.parser.utcode import UTCode

__all__ = ['TestPayload']

class TestPayload(Base):
    def __init__(self):
        self.ioengine.loop.add_callback(self.consume)

    @property
    def to_encode(self):
        try:
            assert self.__to_encode
        except (AssertionError,AttributeError):
            self.__to_encode = [
                {
                    'uid':'h_umgeher',
                    'request':{
                        'url':'http://umgeher.org',
                        'method':'GET'
                    }
                },
                {
                    'uid':'h_freebsd',
                    'request':{
                        'url':'http://freebsd.org',
                        'method':'GET'
                    }
                }
            ]
        except:
            raise
        return iter(self.__to_encode)

    def consume(self,_=None):
        try:
            i = _.next()
        except AttributeError:
            self.ioengine.loop.add_callback(
                self.consume,
                _=self.to_encode
            )
        except StopIteration:
            self.ioengine.stop()
        except:
            raise
        else:
            def on_encode(result):
                self.log.info(result.result())
                self.ioengine.loop.add_callback(
                    self.consume,
                    _=_
                )
                return True
            
            UTCode().encode(i,future=self.ioengine.future(on_encode))
        return True
