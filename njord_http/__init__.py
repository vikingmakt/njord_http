from announce import Announce
from rask.main import Main
from rask.options import define,options
from rask.parser.json import dictfy
from rask.rmq import RMQ
from request import Request
from test_payload import TestPayload

__all__ = ['Run']

class Run(Main):
    __settings = {
        'code':{
            'request':{
                'ok':'2d3fe42a50074ac790bc3bea25a29e7c'
            }
        },
        'rmq_exchange':{
            'topic':'njord_http',
            'headers':'njord_http_headers',
            'headers_any':'njord_http_headers_any'
        },
        'services':{
            'request':{
                'queue':'njord_http_request',
                'rk':'njord.http.request'
            }
        }
    }

    @property
    def services(self):
        try:
            assert self.__services
        except (AssertionError,AttributeError):
            self.__services = {}
        except:
            raise
        return self.__services
    
    def after(self):
        self.log.info('Njord HTTP')
        try:
            assert options.payload
        except AssertionError:
            self.ioengine.loop.add_callback(self.settings_load)
        except:
            raise
        else:
            self.ioengine.loop.add_callback(TestPayload)
        return True
    
    def before(self):
        define('payload',default=False)
        return True

    def rabbitmq(self):
        self.rmq = RMQ(url=options.http['rmq'])
        self.rmq.channel('announce',future=self.ioengine.future(Announce))
        self.ioengine.loop.add_callback(self.services_init)
        return True

    def services_init(self):
        def on_request(_):
            self.services['request'] = Request(channel=_)
            self.log.info('Service: request')
            return True
        
        self.rmq.channel('request',future=self.ioengine.future(on_request))
        return True
    
    def settings_check(self,_=None):
        try:
            assert _.next() in options.http
        except AssertionError:
            self.log.error('No valid settings!')
            self.ioengine.stop()
            return False
        except AttributeError:
            self.ioengine.loop.add_callback(
                self.settings_check,
                _=iter([
                    'rmq'
                ])
            )
        except StopIteration:
            self.ioengine.loop.add_callback(self.rabbitmq)
        except:
            raise
        else:
            self.ioengine.loop.add_callback(self.settings_check,_)
        return None
    
    def settings_load(self):
        try:
            rfile = open('settings.json','r')
            rconf = dictfy(rfile.read())
            rfile.close()
            assert rconf
        except AssertionError:
            self.log.error('No valid json!')
            self.ioengine.stop()
        except IOError:
            self.log.error('No settings file found!')
            self.ioengine.stop()
        except:
            raise
        else:
            rconf.update(self.__settings)
            define('http',default=rconf)
            self.ioengine.loop.add_callback(self.settings_check)
        return True
